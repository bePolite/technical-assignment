<?php

namespace App\Weather\Services;

use DavidePastore\Ipinfo\Host;
use DavidePastore\Ipinfo\Ipinfo;
use Illuminate\Cache\Repository;
use Illuminate\Foundation\Application;
use Illuminate\Http\Request;

class IpInfoService
{
    const ENV_LOCAL = 'local';

    /**
     * @var Ipinfo
     */
    private $ipInfoClient;

    /**
     * @var Repository
     */
    private $cacheRepository;

    /**
     * @var Application
     */
    private $application;

    /**
     * @param Ipinfo      $ipInfoClient
     * @param Repository  $cacheRepository
     * @param Application $application
     */
    public function __construct(Ipinfo $ipInfoClient, Repository $cacheRepository, Application $application)
    {
        $this->ipInfoClient    = $ipInfoClient;
        $this->cacheRepository = $cacheRepository;
        $this->application     = $application;
    }

    /**
     * @param Request $request
     *
     * @return Host
     */
    public function getDefaultHostForRequest(Request $request): Host
    {
        if ($this->application->environment(self::ENV_LOCAL)) {
            return $this->cacheRepository->rememberForever($request->getClientIp(), function () {
                return $this->ipInfoClient->getYourOwnIpDetails();
            });
        }

        return $this->cacheRepository->rememberForever($request->getClientIp(), function () use ($request) {
            return $this->ipInfoClient->getIpGeoDetails($request->getClientIp());
        });
    }
}

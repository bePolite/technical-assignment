# Weather App

### Introduction
This is an application that allows a user to view the observed (in the past 30 days) or forecasted (in the future) daily weather conditions for a given location using the [Dark Sky API](https://darksky.net/dev/docs).

**Demo URL:** [https://darksky-app.herokuapp.com](https://darksky-app.herokuapp.com/)


### Tech Stack

- The backend of the app is built using PHP 7.2 and the Laravel 5. Since the app has to call the Dark Sky API to get weather data, I used guzzle (`guzzlehttp/guzzle`) as an http client for API calls

- The Frontend of the app is built using tailwindcss, webpack and JQuery. I intend to use VeuJS to do the frontend in the near future.

- PHPUnit is used for both unit tests and integration tests. (PS: It's advisable to run the tests using docker) 


### Continuous Integration / Continuous Deployment

Bitbucket pipelines are used for CI/CD. Each time a push is made into the `master` branch, the bitbucket pipelines is run and if all the tests pass, then the application is automatically deployed to `heroku`


### Local Setup

Follow the steps below to run this application locally

1 - Clone this git repository and `cd` into it

```bash
$ git clone git@bitbucket.org:bePolite/technical-assignment.git
$ cd technical-assignment
```

2 - Copy the `.env.example` file into `.env`

```bash
$ cp .env.example .env
```

3 - Get an API key for IP info from `https://ipinfo.io/account` and assign it to this variable in the `.env` file `IPINFO_ACCESS_TOKEN=`

4 - Get your API key for DarkSky and assign set the `DARK_SKY_API_ENDPOINT` variable in your `.env` file. It should look something like this `DARK_SKY_API_ENDPOINT='https://api.darksky.net/forecast/asdfdsqw2323ddsssdfd/'`

5 - Run the docker container

```bash
$ docker-compose up -d
```

6 - Run the bash shell of the workspace docker container

```bash
$ docker exec -it dark-sky-workspace /bin/bash
```

7 - Install `composer` dependencies and `npm` dependencies with `yarn` inside the docker container

```bash
$ composer install
$ yarn
$ npm run dev
```

8 - Generate the laravel application key

```bash
$ php artisan key:generate
```

9 - Open your browser and visit localhost: [http://localhost:8080](http://localhost:8080).

### Running Tests

To run tests, setup the application using the setup process shown above and run `phpunit` inside the workspace container


```bash
$ docker exec -it dark-sky-workspace ./vendor/bin/phpunit --testdox
```

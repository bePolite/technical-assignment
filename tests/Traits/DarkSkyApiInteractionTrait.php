<?php

namespace Tests\Traits;

trait DarkSkyApiInteractionTrait
{
    /**
     * @return string
     */
    protected function getDummyApiResponse(): string
    {
        return file_get_contents(
            base_path()
            . DIRECTORY_SEPARATOR
            . 'tests'
            . DIRECTORY_SEPARATOR
            . 'data'
            . DIRECTORY_SEPARATOR
            . 'darksky-api-data.json'
        );
    }
}

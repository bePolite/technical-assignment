<?php

namespace Tests\Unit\Weather\Services;

use App\Weather\Clients\DarkSkyApiClient;
use App\Weather\Contracts\LocationDateTimeInput;
use App\Weather\DTOs\DailyWeatherData;
use App\Weather\Services\DarkSkyWeatherDataService;
use App\Weather\Transformers\DarkSkyJsonDataToWeatherDataTransformer;
use PHPUnit_Framework_MockObject_MockObject;
use Tests\TestCase;
use Tests\Traits\DarkSkyApiInteractionTrait;

class DarkSkyWeatherDataServiceTest extends TestCase
{
    use DarkSkyApiInteractionTrait;

    /**
     * @var DarkSkyApiClient|PHPUnit_Framework_MockObject_MockObject
     */
    private $darkSkyApiClient;

    /**
     * @var DarkSkyJsonDataToWeatherDataTransformer|PHPUnit_Framework_MockObject_MockObject
     */
    private $darkSkyJsonDataToWeatherDataTransformer;

    /**
     * @var LocationDateTimeInput|PHPUnit_Framework_MockObject_MockObject
     */
    private $locationDateTimeInput;

    /**
     * @var DailyWeatherData
     */
    private $dailyWeatherData;

    /**
     * @var DarkSkyWeatherDataService
     */
    private $SUT;

    protected function setUp()
    {
        parent::setUp();

        $this->dailyWeatherData = $this->getMockForConcreteClass(DailyWeatherData::class);

        $this->locationDateTimeInput = $this->getMockForConcreteClass(LocationDateTimeInput::class);

        $this->darkSkyApiClient = $this->getMockForConcreteClass(DarkSkyApiClient::class);
        $this->darkSkyApiClient
            ->method($this->methodName([$this->darkSkyApiClient, 'fetchWeatherData']))
            ->willReturn(json_decode($this->getDummyApiResponse()));

        $this->darkSkyJsonDataToWeatherDataTransformer = $this->getMockForConcreteClass(DarkSkyJsonDataToWeatherDataTransformer::class);
        $this->darkSkyJsonDataToWeatherDataTransformer
            ->method($this->methodName([$this->darkSkyJsonDataToWeatherDataTransformer, 'transform']))
            ->willReturn($this->dailyWeatherData);

        $this->dailyWeatherData = $this->getMockForConcreteClass(DailyWeatherData::class);

        $this->SUT = new DarkSkyWeatherDataService(
            $this->darkSkyApiClient,
            $this->darkSkyJsonDataToWeatherDataTransformer
        );
    }

    public function test_that_it_returns_an_instance_of_the_daily_weather_data()
    {
        $dailyWeatherData = $this->SUT->getDailyWeatherData($this->locationDateTimeInput);

        $this->assertInstanceOf(DailyWeatherData::class, $this->dailyWeatherData);

        $this->assertEquals($this->dailyWeatherData, $dailyWeatherData);
    }
}

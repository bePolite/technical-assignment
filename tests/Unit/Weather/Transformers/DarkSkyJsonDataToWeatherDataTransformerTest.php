<?php

namespace Tests\Feature\Weather\Transformers;

use App\Weather\DTOs\DailyWeatherData;
use App\Weather\Transformers\DarkSkyJsonDataToWeatherDataTransformer;
use stdClass;
use Tests\TestCase;

class DarkSkyJsonDataToWeatherDataTransformerTest extends TestCase
{
    /**
     * @var stdClass
     */
    private $weatherJsonData;

    /**
     * @var DarkSkyJsonDataToWeatherDataTransformer
     */
    private $SUT;

    protected function setUp()
    {
        parent::setUp();

        $this->weatherJsonData = $this->getWeatherJsonData();

        $this->SUT = new DarkSkyJsonDataToWeatherDataTransformer();
    }

    public function test_that_the_transformer_returns_a_daily_weather_data_instance()
    {
        $dailyWeatherData = $this->SUT->transform($this->weatherJsonData);

        $this->assertInstanceOf(DailyWeatherData::class, $dailyWeatherData);
    }

    public function test_that_the_transformer_produces_a_daily_weather_data_instance_with_the_correct_data()
    {
        $dailyWeatherData = $this->SUT->transform($this->weatherJsonData);

        $expectedWeatherData = [
            'morning' => [
                'summary'             => 'Humid',
                'iconUrl'             => 'http://localhost/images/animated/clear-day.svg',
                'temperature'         => 23.87,
                'apparentTemperature' => 24.93,
                'humidity'            => 100,
                'windSpeed'           => 0,
            ],
            'afternoon' => [
                'summary'             => 'Humid',
                'iconUrl'             => 'http://localhost/images/animated/clear-day.svg',
                'temperature'         => 30.89,
                'apparentTemperature' => 36.38,
                'humidity'            => 67,
                'windSpeed'           => 2.1,
            ],
            'evening' => [
                'summary'             => 'Humid',
                'iconUrl'             => 'http://localhost/images/animated/clear-night.svg',
                'temperature'         => 25.88,
                'apparentTemperature' => 27.73,
                'humidity'            => 89,
                'windSpeed'           => 3.11,
            ],
        ];

        $this->assertSame(
            json_encode($expectedWeatherData),
            json_encode($dailyWeatherData)
        );
    }

    /**
     * @return stdClass
     */
    private function getWeatherJsonData()
    {
        return json_decode(json_encode([
            'hourly' => [
                'data' => [
                    [],
                    [],
                    [],
                    [],
                    [],
                    [],
                    [],
                    [
                        'summary'             => 'Humid',
                        'icon'                => 'clear-day',
                        'temperature'         => 23.87,
                        'apparentTemperature' => 24.93,
                        'humidity'            => 1,
                        'windSpeed'           => 0,
                    ],
                    [],
                    [],
                    [],
                    [],
                    [],
                    [
                        'summary'             => 'Humid',
                        'icon'                => 'clear-day',
                        'temperature'         => 30.89,
                        'apparentTemperature' => 36.38,
                        'humidity'            => .67,
                        'windSpeed'           => 2.1,
                    ],
                    [],
                    [],
                    [],
                    [],
                    [],
                    [
                        'summary'             => 'Humid',
                        'icon'                => 'clear-night',
                        'temperature'         => 25.88,
                        'apparentTemperature' => 27.73,
                        'humidity'            => 0.89,
                        'windSpeed'           => 3.11,
                    ],
                ],
            ],
        ]));
    }
}

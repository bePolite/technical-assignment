<?php

namespace Tests\Unit\Weather\Configurations;

use App\Weather\Configurations\DarkSkyApiConfiguration;
use InvalidArgumentException;
use Tests\TestCase;

class DarkSkyApiConfigurationTest extends TestCase
{
    /**
     * @var string
     */
    private $validApiEndpoint = 'https://api.darksky.net/forecast/b95b5555fb5f8e94cf499f4036618e55/';

    /**
     * @var string
     */
    private $units = 'si';

    /**
     * @var array
     */
    private $excludedBlocks = ['daily', 'currently'];

    /**
     * @var DarkSkyApiConfiguration
     */
    private $SUT;

    /**
     * @param string $invalidEndpointUrl
     *
     * @dataProvider provideNonUrlStrings
     */
    public function test_that_an_invalid_argument_exception_is_thrown_when_the_api_endpoint_is_not_a_url(
        string $invalidEndpointUrl
    ) {
        $this->expectException(InvalidArgumentException::class);

        $this->SUT = new DarkSkyApiConfiguration($this->excludedBlocks, $invalidEndpointUrl, $this->units);
    }

    public function test_that_the_get_api_endpoint_method_returns_the_endpoint_url()
    {
        $this->SUT = new DarkSkyApiConfiguration($this->excludedBlocks, $this->validApiEndpoint, $this->units);

        $this->assertEquals($this->validApiEndpoint, $this->SUT->getApiEndpoint());
    }

    public function test_that_the_get_excluded_blocks_method_returns_the_endpoint_url()
    {
        $this->SUT = new DarkSkyApiConfiguration($this->excludedBlocks, $this->validApiEndpoint, $this->units);

        $this->assertEquals($this->excludedBlocks, $this->SUT->getExcludedBlocks());
    }

    public function test_that_the_get_units_method_returns_the_endpoint_url()
    {
        $this->SUT = new DarkSkyApiConfiguration($this->excludedBlocks, $this->validApiEndpoint, $this->units);

        $this->assertEquals($this->units, $this->SUT->getUnits());
    }

    /**
     * @return array
     */
    public function provideNonUrlStrings()
    {
        return [
            ['edasdfsd'],
            [DarkSkyApiConfiguration::class],
            ["\n\n"],
            [213223],
            ['233edasdfsd'],
        ];
    }
}

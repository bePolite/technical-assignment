<?php

namespace Tests\Feature\Weather\Stubs;

use App\Weather\Clients\DarkSkyApiClient;
use App\Weather\Contracts\LocationDateTimeInput;
use stdClass;

class DarkSkyApiClientStub extends DarkSkyApiClient
{
    /**
     * @var stdClass
     */
    private $jsonWeatherData;

    /**
     * @param stdClass $jsonWeatherData
     */
    public function __construct(stdClass $jsonWeatherData)
    {
        $this->jsonWeatherData = $jsonWeatherData;
    }

    /**
     * {@inheritdoc}
     */
    public function fetchWeatherData(LocationDateTimeInput $locationDateTimeInput): stdClass
    {
        return $this->jsonWeatherData;
    }
}

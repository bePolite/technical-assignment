<?php

namespace Tests\Feature\Weather;

use App\Weather\Clients\DarkSkyApiClient;
use Illuminate\Http\Response;
use Tests\Feature\Weather\Stubs\DarkSkyApiClientStub;
use Tests\TestCase;
use Tests\Traits\DarkSkyApiInteractionTrait;

class WeatherApiTest extends TestCase
{
    use DarkSkyApiInteractionTrait;

    /**
     * @var float
     */
    private $latitude = 37.8267;

    /**
     * @var int
     */
    private $longitude = -122.4233;

    /**
     * @var string
     */
    private $date;

    /**
     * @var string
     */
    private $apiEndpoint = 'api/weather';

    protected function setUp()
    {
        parent::setUp();

        $this->date = now()->toDateString();

        $this->bindDarkskyApiClientStub();
    }

    public function test_that_the_weather_api_returns_an_error_if_inputs_are_not_supplied()
    {
        $this->json('POST', $this->apiEndpoint)
            ->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY)
            ->assertJson([
                'latitude'  => ['The latitude field is required.'],
                'longitude' => ['The longitude field is required.'],
                'date'      => ['The date field is required.'],
            ]);
    }

    /**
     * @param string $date
     *
     * @dataProvider provideDatesInThePast
     */
    public function test_that_the_weather_api_returns_an_error_if_the_supplied_date_is_less_30_days_from_the_current_date(
        string $date
    ) {
        $payload = $this->getPayload($this->latitude, $this->longitude, $date);

        $this->json('POST', $this->apiEndpoint, $payload)
            ->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY)
            ->assertJson([
                'date' => ['The date must be a date after ' . now()->subDays(31)->toDateString() . '.'],
            ]);
    }

    /**
     * @param string $date
     *
     * @dataProvider provideDatesInTheFuture
     */
    public function test_that_the_weather_api_returns_an_error_if_the_supplied_date_is_more_than_7_days_in_the_future(
        string $date
    ) {
        $payload = $this->getPayload($this->latitude, $this->longitude, $date);

        $this->json('POST', $this->apiEndpoint, $payload)
            ->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY)
            ->assertJson([
                'date' => ['The date must be a date before ' . now()->addDays(8)->toDateString() . '.'],
            ]);
    }

    public function test_that_the_weather_api_returns_the_correct_data_if_the_correct_parameters_are_provided()
    {
        $payload = $this->getPayload($this->latitude, $this->longitude, $this->date);

        $this->json('POST', $this->apiEndpoint, $payload)
            ->assertStatus(Response::HTTP_OK)
            ->assertJson([
                'code'    => Response::HTTP_OK,
                'payload' => [
                    'morning' => [
                        'summary'             => 'Humid',
                        'iconUrl'             => 'http://localhost/images/animated/clear-day.svg',
                        'temperature'         => 23.87,
                        'apparentTemperature' => 24.93,
                        'humidity'            => 100,
                        'windSpeed'           => 0,
                    ],
                    'afternoon' => [
                        'summary'             => 'Humid',
                        'iconUrl'             => 'http://localhost/images/animated/clear-day.svg',
                        'temperature'         => 30.89,
                        'apparentTemperature' => 36.38,
                        'humidity'            => 67,
                        'windSpeed'           => 2.1,
                    ],
                    'evening' => [
                        'summary'             => 'Humid',
                        'iconUrl'             => 'http://localhost/images/animated/clear-night.svg',
                        'temperature'         => 25.88,
                        'apparentTemperature' => 27.73,
                        'humidity'            => 89,
                        'windSpeed'           => 3.11,
                    ],
                ],
            ]);
    }

    /**
     * @return array
     */
    public function provideDatesInThePast()
    {
        return [
            [now()->subDays(31)->toDateString()],
            [now()->subDays(331)->toDateString()],
            [now()->subDays(61)->toDateString()],
        ];
    }

    /**
     * @return array
     */
    public function provideDatesInTheFuture()
    {
        return [
            [now()->addDays(8)->toDateString()],
            [now()->addDays(331)->toDateString()],
            [now()->addDays(61)->toDateString()],
        ];
    }

    /**
     * @param float  $latitude
     * @param float  $longitude
     * @param string $date
     *
     * @return array
     */
    private function getPayload(float $latitude, float $longitude, string $date): array
    {
        return [
            'latitude'  => $latitude,
            'longitude' => $longitude,
            'date'      => $date,
        ];
    }

    private function bindDarkSkyApiClientStub()
    {
        $this->app->singleton(DarkSkyApiClient::class, function () {
            return  new DarkSkyApiClientStub(json_decode($this->getDummyApiResponse()));
        });
    }
}

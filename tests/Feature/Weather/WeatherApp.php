<?php

namespace Tests\Feature\Weather;

use Tests\TestCase;

class WeatherApp extends TestCase
{
    /**
     * A basic test example.
     */
    public function test_that_the_index_route_is_accessible()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }
}

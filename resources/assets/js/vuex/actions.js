import {
    CHANGE_DATE, CHANGE_DATE_PICKER_END_DATE,
    CHANGE_DATE_PICKER_START_DATE,
    CHANGE_LATITUDE,
    CHANGE_LONGITUDE,
    CHANGE_PLACE, CHANGE_WEATHER_API_URL, FETCH_DAILY_WEATHER_DATA
} from './mutation_types'

export default {
    fetchDailyWeather(store) {
        store.commit(FETCH_DAILY_WEATHER_DATA);
    },
    changePlace: (store, place) => {
        store.commit(CHANGE_PLACE, place);
    },
    changeLongitude: (store, longitude) => {
        store.commit(CHANGE_LONGITUDE, longitude);
    },
    changeLatitude: (store, latitude) => {
        store.commit(CHANGE_LATITUDE, latitude);
    },
    changeDate: (store, date) => {
        store.commit(CHANGE_DATE, date);
    },
    changeDatePickerStartDate: (store, date) => {
        store.commit(CHANGE_DATE_PICKER_START_DATE, date);
    },
    changeDatePickerEndDate: (store, date) => {
        store.commit(CHANGE_DATE_PICKER_END_DATE, date);
    },
    changeWeatherApiUrl: (store, url) => {
        store.commit(CHANGE_WEATHER_API_URL, url);
    }
}

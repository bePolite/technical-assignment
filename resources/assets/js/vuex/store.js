import Vue from 'vue'
import Vuex from 'vuex'
import getters from './getters'
import actions from './actions'
import mutations from './mutations'

Vue.use(Vuex);

const state = {
    isFetchingWeatherData: false,
    date: null,
    datePickerStartDate: null,
    datePickerEndDate: null,
    weatherApiUrl: null,
    place: null,
    longitude: null,
    latitude: null,
    dailyWeatherData: null,
};

export default new Vuex.Store({
    state,
    mutations,
    getters,
    actions
})
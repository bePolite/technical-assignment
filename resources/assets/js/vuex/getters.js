export default {
    getLongitude: state => state.longitude,
    getLatitude: state => state.latitude,
    getDate: state => state.date,
    getPlace: state => state.place,
    getDatePickerStartDate: state => state.datePickerStartDate,
    getDatePickerEndDate: state => state.datePickerEndDate,
    getWeatherApiUrl: state => state.weatherApiUrl,
    getIsFetchingWeatherData: state => state.isFetchingWeatherData,
    getDailyWeatherData: state => state.dailyWeatherData,
}
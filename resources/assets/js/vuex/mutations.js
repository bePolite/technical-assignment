import * as types from './mutation_types'
import getters from './getters';
import axios from 'axios';

export default {
    [types.CHANGE_LATITUDE] (state, latitude) {
        state.latitude = latitude;
    },
    [types.CHANGE_LONGITUDE] (state, longitude) {
        state.longitude = longitude;
    },
    [types.CHANGE_DATE] (state, rawDate) {
        let date = new Date(rawDate);
        state.date = date.getFullYear() + '-' + ('0' + (date.getMonth()+1)).slice(-2) + '-' + ('0' + date.getDate()).slice(-2);
    },
    [types.CHANGE_PLACE] (state, place) {
        state.place = place;
    },
    [types.CHANGE_DATE_PICKER_START_DATE] (state, date) {
        state.datePickerStartDate = date;
    },
    [types.CHANGE_DATE_PICKER_END_DATE] (state, date) {
        state.datePickerEndDate = date;
    },
    [types.CHANGE_WEATHER_API_URL] (state, url) {
        state.weatherApiUrl = url;
    },
    [types.FETCH_DAILY_WEATHER_DATA] (state) {
        let longitude = getters.getLongitude(state);
        let latitude = getters.getLatitude(state);
        let date = getters.getDate(state);
        let places = getters.getPlace(state);

        if(!longitude || !latitude || !date || !places) {
            return
        }

        state.isFetchingWeatherData = true;
        axios.post(getters.getWeatherApiUrl(state), {
            'longitude': getters.getLongitude(state),
            'latitude': getters.getLatitude(state),
            'date': getters.getDate(state)
        })
        .then((response) => {
            state.isFetchingWeatherData = false;
            state.dailyWeatherData = response.data.payload;
        });
    },
}